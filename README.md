# Middlemartian

## Install and usage

To install the template clone the repo to the `~/.middleman` dir:

```
git clone https://gitlab.com/marsbot/Middlemartian.git ~/.middleman/middlemartian
```

After installing the template, you can use it with:

```
middleman init PROJECT_NAME -T middlemartian
```

**Note:** Update the template frecuently

## Middleman features

- Blog
- Custom template helpers
- Facebook Open Graph
- Favicon maker
- HTML minifier
- Live reload
- Optimized for SEO
- Sass linting
- Site map
- Slim engine
- Twitter Cards

## Javascript features

- Babel
- Eslint
- ES6
- UglyfyJS
- Webpack

## General features

- Gitlab to S3 deploy system
- Image optimizer
- Off-line service
